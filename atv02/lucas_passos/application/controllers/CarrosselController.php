<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class CarrosselController extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('CarrosselModel', 'carrossel');
    }

    public function index(){
        $data['carrossel'] = $this->carrossel->carrossel();
        $html = $this->load->view('carrosselView', $data, true);
        $this->show($html);
    }

}