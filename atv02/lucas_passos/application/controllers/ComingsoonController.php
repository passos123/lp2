<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ComingsoonController extends MY_Controller{


    public function index(){
        $this->load->model('ComingsoonModel', 'comingsoon');
        $data['comingsoon'] = $this->comingsoon->Comingsoon();
        $html = $this->load->view('ComingsoonView', $data, true);
        $this->show($html);
    }

}