<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ComponentesController extends MY_Controller{

    /**
     * Função index para carregar a página index
     */
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('common/IndexView');
        $this->load->view('common/footer');
        
    }

    /**
     * Função Carrossel para carregar a página do componente carrossel
     */
    public function Carrossel(){

        $this->load->model('CarrosselModel', 'carrossel');
        $data['carrossel'] = $this->carrossel->carrossel();
        $html = $this->load->view('carrosselView', $data, true);
        $this->show($html);
    }

    /**
     * Função Navbar para carregar a página do componente Navbar
     */
    public function Navbar(){
        
        $this->load->model('NavbarModel', 'nav');
        $data['navbar'] = $this->nav->navbar();
        $html = $this->load->view('navbarView', $data, true);
        $this->show($html);

    }

    /**
     * Função Comingsoon para carregar a página do componente Comingsoon
     */
    public function Comingsoon(){
        $this->load->model('ComingsoonModel', 'comingsoon');
        $data['comingsoon'] = $this->comingsoon->Comingsoon();
        $html = $this->load->view('ComingsoonView', $data, true);
        $this->show($html);
    }

}