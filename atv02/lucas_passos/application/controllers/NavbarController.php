<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class NavbarController extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('NavbarModel', 'nav');
    }

    public function index(){
        $data['navbar'] = $this->nav->navbar();
        $html = $this->load->view('navbarView', $data, true);
        $this->show($html);
    }

}