<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');

include_once APPPATH . 'libraries/component/Carrossel.php';

class CarrosselTest extends MyToast{

    function __construct(){
        parent::__construct('Comingsoon');
    }
/**
 * Teste unitário para validação de alteração da imagem
 */
    function test_img(){
        $carrossel = new Carrossel();
        $carrossel->setImg(20);
        $res = $carrossel->getImg();
        $this->_assert_equals('20',$res, "Erro: esperado (20),recebido ($res)");

    }

/**
 * Teste para validar se o número de imagem passado é válido para o bootstrap
 */
    function test_sizeImg(){
        $carrossel = new Carrossel();
        $carrossel->setImg(100);
        $res = $carrossel->getImg();
        $resultado = $res >0 && $res<100? true : false;
        $this->_assert_true($resultado, 'O número da imagem deve ser maior que 0 e menor que 100');
    }

    
}

