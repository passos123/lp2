<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');

include_once APPPATH . 'libraries/component/Comingsoon.php';

class ComingsoonTest extends MyToast{

    function __construct(){
        parent::__construct('Comingsoon');
    }
/**
 * Teste unitário para validação de alteração da imagem
 */
    function test_img(){
        $comingsoon = new Comingsoon();
        $comingsoon->setImg(13);
        $res = $comingsoon->getImg();
        $this->_assert_equals('13',$res, "Erro: esperado (13),recebido ($res)");

    }
/**
 * Teste unitário para validação do título inserido
 */
    function test_title(){
        $comingsoon = new Comingsoon();
        $comingsoon->setTitle('ABC');
        $res = $comingsoon->getTitle();
        $this->_assert_equals('-1',$res, "Erro: esperado uma string maior que 3 caracteres");

    }
/**
 * Teste para validar se o número de imagem passado é válido para o bootstrap
 */
    function test_sizeImg(){
        $comingsoon = new Comingsoon();
        $comingsoon->setImg(0);
        $res = $comingsoon->getImg();
        $resultado = $res >0 && $res<100? true : false;
        $this->_assert_true($resultado, 'O número da imagem deve ser maior que 0 e menor que 100');
    }

    
}

