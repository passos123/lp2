<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');

include_once APPPATH . 'libraries/component/Navbar.php';

class NavbarTest extends MyToast{

    function __construct(){
        parent::__construct('Navbar');
    }
/**
 * Teste unitário para verificar se o href foi especificado
 */
    function test_href(){
        $nav = new navbar();
        $nav->setHref('');
        $res = $nav->getHref();
        $valida = $res == null ? true : false;
        $this->_assert_equals(null,$valida, "Erro: link (url) não especificada");

    }
/**
 * Teste unitário para validação do título inserido
 */
    function test_title_size(){
        $nav = new navbar();
        $nav->setTitle('asd');
        $res = $nav->getTitle();
        $this->_assert_equals('-1',$res, "Erro: esperado uma string maior que 4 caracteres");

    }

/**
 * Teste unitário para validação do título , verificando se o titulo inserido é uma string
 */
function test_title_string(){
    $nav = new navbar();
    $nav->setTitle('qwer');
    $res = $nav->getTitlenumeric();
    $this->_assert_false($res, "Erro: esperado uma string");

}

    
}

