<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');

include_once APPPATH . 'libraries/Product.php';
class ProductTest extends MyToast{

    function __construct(){
        parent::__construct('ProductTest');
    }
    //Caso  de teste 1
    function test_nome_eh_obrigatorio(){
        $prod = new Product('Celular');
        $nome = $prod->getNome();
        $this->_assert_equals('Celular',$nome, "Erro: esperado (Celular),recebido ($nome)");
    }

    //Caso de teste 2
    function test_nome_maior_tres_caracteres(){
        //cenario 1
        $prod1 = new Product('');
        $nome1 = $prod1->getNome();
        $result = $nome1 == -1;
        $this->_assert_true($result, 'O nome deve ter, pleo menos, 3 chars');

        //cenario 2
        $prod2 = new Product('Uva');
        $nome2 = $prod2->getNome();
        $result = $nome2 != -1;
        $this->_assert_true($result, 'O nome deve ter, pleo menos, 3 chars');
    }

    //caso de teste 3
    function test_preco_nao_eh_nulo(){
        $prod = new Product('abcd');
        $preco = $prod->getPreco();
        $result = $preco > 0;
        $this->_assert_false($result,' O preço não pode ser nulo');
    }

    //caso de teste 4
    function test_preco_eh_dinamico(){
        $prod = new Product('efgh');
        $preco = $prod->getPreco();
        $this->_assert_equals_strict(0, $preco, 'Preço Inicial deve ser zero');

        $prod->setPreco(678.9);
        $res= $prod->getPreco();
        $this->_assert_equals(678.9, $res, 'Erro na atribuição do preço');
    }

    //Caso de teste 5
     function test_cacula_desconto(){
         $prod = new Product('ijkl');
         $prod->setPreco(300);
         $desc = $prod -> desconto(15);
         $this->_assert_equals(45, $desc, 'Erro no cálculo do desconto');
     }
}