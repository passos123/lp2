<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller{

    public function show($conteudo){
        // incluir o cabeçalho
        $html = $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbar', null, true);

        // incluir o conteúdo
        //$html .= '<div class="container">';
        $html .= $conteudo;
        //$html .= '</div>';

        // incluir o rodapé
        //$html .= $this->load->view('componente/rodape', null, true);
        $html .= $this->load->view('common/footer', null, true);
        echo $html;
    }

}