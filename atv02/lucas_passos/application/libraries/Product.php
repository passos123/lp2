<?php
/**
 * Classe que representa os produtos do e-commerce da empresa IFSP.
 */
class Product {
    private $nome;
    private $preco=0;

    function __construct($nome){
        $this->nome=$nome;

    }
/**
 * Informa o nome do produto.
 * @return nome:string;
 */
    public function getNome(){
        $size = strlen($this->nome);
        return $size >= 3 ? $this-> nome : -1;
    }
/**
 * Informa o preço do produto.
 * @return preco: flaot > 0
 */
    public function getPreco(){
        return $this->preco;
    }
/**
 * Calcula um desconto percentual sobre o preco do produto
 * @param percent: inteiro 
 * @return desconto: float
 */
public function desconto($percent){
    $desc = $this->preco * $percent/100;
    return $desc;
}

    
/**
 *  Atribui preço ao produto.
 * @param preco: float
 * @param prazo : date9
 */
    public function setPreco($preco){
        $this->preco =  $preco;
    }
}