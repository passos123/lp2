<?php 
include_once 'Component.php';

class ActionButton extends Component {

    protected $id;

    public function __construct($id) {
        $this->id = $id;
    }

    public function getHTML(){
        $html = '<a href="'.base_url('funcionario/edita/'.$this->id).'"><i class="fas fa-edit mr-3 blue-text title="Editar"></i>';
        $html .= '<a href="'.base_url('funcionario/delete/'.$this->id).'"><i class="fas fa-trash-alt mr-3 red-text title="Deletar"></i>';
        return $html;
    }
}