<?php
include_once 'Component.php';

class Carrossel extends Component {
    /**
     * Declaração das variaveis do componente
     */
    private $botoes='';
    private $img='';
    private $conteudo = '<div class="carousel-item active">
    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(35).jpg">
</div>';

    /**
     * Gera o header 
     * @return header:string
     */

    private function header(){
        $html = '<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">';
        $html .= '<div class="carousel-inner">';
        return $html;
    }

    /**
     * Retorna o conteudo
     * @return content:string
     */
    private function content(){
        $html = $this->conteudo;
        return $html;
    }

    /** 
     * Gera o código HTML
     * @return getHTML:string  
     */ 
    public function getHTML(){
        $html = $this->header();
        $html .= $this->content();
        $html.='</div>'.$this->botoes.'</div>';
        return $html;
    }

    /**
     * Função para carregar os botões do carrossel
     * @return carrossel_botoes:string
     */
    public function corrossel_botoes(){
        $this->botoes = '<a class="carousel-control-prev" href="#carouselExampleSlidesOnly" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleSlidesOnly" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>';
        return $this;
    }

   
    public function setContent(){
        $this->conteudo .= '<div class="carousel-item">
        <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20('.$this->img.').jpg">
    </div>';
    }

    /**
     * Função para definir a imagem
     * @param img
     */
    public function setImg($img){
        $this->img = $img;

        return $this;
    }
    

    /**
     * Informa o número de imagem da página .
     * @return Img;
     */
    public function getImg(){
        return $this->img;
    }

}
