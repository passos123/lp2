<?php
include_once 'Component.php';

class Comingsoon extends Component {
  /**
   * declaração das variaveis com valores pré-definidos 
   */
    private $data='Jan 5, 2021 15:37:25';
    private $title='Coming Brunão';
    private $mensagem='Mensagem de teste';
    private $img='35';

    /**
     * Função para retornar o css
     * @return css:string
     */
    private function css(){
        $html='<style>
        body, html {
          height: 100%;
          margin: 0;
        }
        
        .bgimg {
          background-image: url("https://mdbootstrap.com/img/Photos/Slides/img%20('.$this->img.').jpg");
          height: 100%;
          background-position: center;
          background-size: cover;
          position: relative;
          color: white;
          font-family: "Courier New", Courier, monospace;
          font-size: 25px;
        }
        
        .topleft {
          position: absolute;
          top: 0;
          left: 16px;
        }
        
        .bottomleft {
          position: absolute;
          bottom: 0;
          left: 16px;
        }
        
        .middle {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          text-align: center;
        }
        
        hr {
          margin: auto;
          width: 40%;
        }
        </style> ';

        return $html;
    }

    /**
     * Função para retornar o javascript
     * @return javascript:string
     */
    private function javascript(){
        $html='<script>
        // Set the date were counting down to
        var countDownDate = new Date("'.$this->data.'").getTime();
        
        // Update the count down every 1 second
        var countdownfunction = setInterval(function() {
        
          // Get todays date and time
          var now = new Date().getTime();
          
          // Find the distance between now an the count down date
          var distance = countDownDate - now;
          
          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
          
          // Output the result in an element with id="demo"
          document.getElementById("demo").innerHTML = days + "d " + hours + "h "
          + minutes + "m " + seconds + "s ";
          
          // If the count down is over, write some text 
          if (distance < 0) {
            clearInterval(countdownfunction);
            document.getElementById("demo").innerHTML = "EXPIRED";
          }
        }, 1000);
        </script>';

        return $html;
    }

    /**
     * Gera o cabeçalho 
     * @return header:string
     */
    private function header(){
        $html ='<div class="bgimg">
        <div class="middle">
          <h1>'.$this->title.'</h1>
            <hr>
                <p id="demo" style="font-size:30px"></p>
            </div>
            <div class="bottomleft">
                <p>'.$this->mensagem.'</p>
            </div>
        </div>';

        return $html;
    }


    /** 
     * Gera o código HTML 
     * @return getHTML:string
     */ 
    public function getHTML(){
       
        $html = $this->css();
        $html .= $this->header();

        $html .= $this->javascript();
        return $html;
    }
   
    /**
     *Define a data
     *@param data:string 
     */

    public function setData($data){
        $this->data = $data;
        return $this;
    }

    /**
     * Define o número da imagem
     * @param img
     */
    public function setImg($img){
        $this->img = $img;
        return $this;
    }

    /**
     * Define o titulo
     * @param title:string
     */

    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Define a mensagem
     * @param mensagem:string
     */
    public function setMensagem($mensagem){
        $this->mensagem = $mensagem;
        return $this;
    }


    /**
     * Informa o número de imagem da página .
     * @return Img;
     */

    public function getImg(){
      return $this->img;
    }
    
    /**
     * Informa o nome do produto.
     * @return title:string;
     */

    public function getTitle(){
      $size = strlen($this->title);
      return $size >= 5 ? -1 : $this->title;
    }


}
