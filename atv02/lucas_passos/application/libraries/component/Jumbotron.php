<?php

include_once 'Component.php';

class Jumbotron extends Component {
    private $link;
    private $titulo; 
    private $subtitulo;
    private $descricao;
    private $rotulo_botao;

    function __construct($data){
        // $this->link = $data['link'];
        $this->titulo = $data['titulo'];
        $this->subtitulo = $data['subtitulo'];
        $this->descricao = $data['descricao'];
        $this->rotulo_botao = $data['rotulo_botao'];
    }

    public function getHTML(){
        $html = '
        <section class="card blue-gradient wow fadeIn" id="intro">
            <div class="card-body text-white text-center py-5 px-5 my-5">
                <h1 class="mb-4"><strong>'.$this->titulo.'</strong></h1>
                <p><strong>'.$this->subtitulo.'</strong></p>
                <p class="mb-4">'.$this->descricao.'</p>
                <a target="_blank" href="'.$this->link.'" class="btn btn-lg">
                    '.$this->rotulo_botao.'
                    <i class="fas fa-graduation-cap ml-2"></i>
                </a>
            </div>
        </section>';
        return $html;
    }

}