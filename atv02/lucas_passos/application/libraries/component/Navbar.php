<?php
include_once 'Component.php';

class Navbar extends Component {
    /**
     * Declaração das variaveis utilizadas no navbar
     */
    private $title='';
    private $href='';
    private $titleLink='CAMPO1';
    private $textColor='navbar-dark';
    private $color='unique-color';
    private $link='';



    /**
     * Gera o cabeçalho do navbar
     * @return string: header
     */
    private function header(){
        $html ='<nav class="navbar navbar-expand-lg '.$this->textColor.' '.$this->color.'">';
        $html.='<a class="navbar-brand" href="'.$this->href.'">'.$this->title.'</a>';
        $html.='<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="basicExampleNav">';
        return $html;
    }


    /** 
     * Gera o código HTML do navbar
     * @return string: código html
     */ 
    public function getHTML(){
        $html = $this->header();
        $html .= $this->navLink();
        return $html. '</div></nav>';
    }

    /**
     * Gera o link de navegação da navbar
     * @return string: Link de navegação
     */
    private function navLink(){
        $html = '<ul class="navbar-nav mr-auto">';
        $html .= $this->link;
        $html .= '</ul>';
        return $html;
    }

    /**
     * Define o link e o nome do link para a navbar
     * @param string: Nome do link e o href
     */
    public function link(){
        $this->link .= '<li class="nav-item"><a class="nav-link" href="'.$this->href.'">'.$this->titleLink.'</a></li>';
        return $this;
    }

    /** 
    * Função para definir o título
    * @param
    */
    public function setTitle($title){
        $this->title = $title;
        return $this;
    }

    /**
     * Função para definir a cor do texto
     * @param
     */
    public function setTextColor($textColor){
        $this->textColor = $textColor;
        return $this;
    }

    /**
     * Função para definir a cor da navbar
     * @param
     */
    public function setColor($color){
        $this->color = $color;
        return $this;
    }

    /**
     * Função para definir o href 
     * @param
     */
    public function setHref($href){
        $this->href = $href;
        return $this;
    }

    /**
     * Função para definir o Título do link
     * @param
     */
    public function addTitleLink($titleLink){
        $this->titleLink = $titleLink;
        return $this;
    }

    /**
     * Exibe o href
     * @return href
     */

    public function getHref(){
        return $this->href;
    }

    /**
    * Informa o tamanho do título
    * @return titulo:boolean;
    */
    public function getTitle(){
    $size = strlen($this->title);
    return $size >= 4 ? -1 : $this->title;
    }

    /**
    * Verificar se o titulo é numerico e retorna.
    * @return titulo:boolean;
    */
    public function getTitlenumeric(){
        $v = is_numeric($this->title);
        return $v ;
    }


}
