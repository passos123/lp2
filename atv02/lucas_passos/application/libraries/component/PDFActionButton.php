<?php
include_once 'ActionButton.php';

class PDFActionButton extends ActionButton {
    
    public function __construct($id){
        parent::__construct($id);
    }

    public function getHTML(){
        $html = '<a href="'.base_url('curriculo/curriculo_'.$this->id).'.pdf")." target="_blank"><i class="far fa-file-pdf mr-3 red-text title="Visualizar Currículo"></i>';
        $html .= parent::getHTML();
        return $html;
    }

}