<?php

class ChatData {
    private $imagem = 'https://mdbootstrap.com/img/Photos/Others/placeholder1.jpg';
    private $titulo;
    private $conteudo;

    function __construct($item){
        $this->titulo = $item->titulo;
        $this->conteudo = $item->conteudo;
    }

    public function imagem(){
        return $this->imagem;
    }

    public function titulo(){
        return $this->titulo;
    }
    
    public function conteudo(){
        return $this->conteudo;
    }

}