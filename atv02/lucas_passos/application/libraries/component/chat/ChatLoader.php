<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'ChatItem.php';
include_once 'ChatData.php';

class ChatLoader extends CI_Object{
    private $chat_item_list;
    
    function __construct($index = 0){
        if($index) $this->getChatData($index);
    }

    private function getChatData($index){
        $sql = "SELECT CONCAT(nome, ' ', sobrenome) AS titulo, 
        imagem, conteudo FROM chat_user, chat_item WHERE 
        chat_list_id = $index AND chat_user.id = user_id";
        $rs = $this->db->query($sql);
        $this->chat_item_list = $rs->result();
    }

    public function getHTML(){
        $html = ''; $i = 0;
        foreach ($this->chat_item_list AS $item) {
            $data = new ChatData($item);
            $item = new ChatItem($data, ($i++ %2 == 0));
            $html .= $item->getHTML();
        }
        return $html;
    }

    public function getSubjectList(){
        $rs = $this->db->get('chat_list');
        return $rs->result();
    }

}