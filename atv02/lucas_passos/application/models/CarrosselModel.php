<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Carrossel.php';


class CarrosselModel extends CI_Model {
    /**
     * Função para instanciar o componente Carrossel e passar seus paramêtros
     */
    public function carrossel(){
        $this->load->library('Carrossel', null, 'carrossel');

        $carrossel = new Carrossel();
        $carrossel->corrossel_botoes()->setImg(38)->setContent();

        return $carrossel->getHTML();
    }

    

}