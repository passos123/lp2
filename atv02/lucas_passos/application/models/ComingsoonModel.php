<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Comingsoon.php';


class ComingsoonModel extends CI_Model {
    /**
     * Função para instanciar o componente Comingsoon e passar seus paramêtros
     */
    public function Comingsoon(){
        $this->load->library('Comingsoon', null, 'comingsoon');

        $comingsoon = new Comingsoon();
        $comingsoon->setMensagem('Mensagem aleatória sobre a página')->setTitle('Título de exemplo')->setImg('44')->setData('Nov 31, 2019 00:00:00');

        return $comingsoon->getHTML();
    }

    

}