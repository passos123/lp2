<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Navbar.php';


class NavbarModel extends CI_Model {

/**
 * Instância do componente Navbar com a definição dos paramêtros necessários.
 *
 */
    public function navbar(){
        $this->load->library('Navbar', null, 'nav');

        $nav = new Navbar();
        $nav->setTitle('Novo Titulo')->setHref('exemplohref')->addTitleLink('titulo1alterado')->link()->setTitle('Titulo 2')->addTitleLink('titulo2alterado')->link()->addTitleLink('link 2')->link()->setColor('info-color-dark')->setTextColor('navbar-dark');

        return $nav->getHTML();
    }

    

}