<br><br><div class="container mx-auto mt-5 ">
<div class="d-flex justify-content-end"><a href="<?php echo site_url('/test/CarrosselTEst')?>"><button type="button" class="btn-sm btn-dark">Teste unitário</button></a></div>
<h1 class="bd-title" id="content">Carrossel</h1>
          <p class="bd-lead">Aprenda a criar um "Carrossel".</p>
</div>
<?php
echo $carrossel;
?>
<div class="container mx-auto mdb-color lighten-5 mt-3">
<code class="language-html" data-lang="html">
<p>Exemplo de utilização:</p>
<span>corrossel_botoes()->setImg(38)->setContent();
</span></code>
</div>
<div class="container mx-auto mt-3">
<h2 id="background-gradient"><span class="bd-content-title"> Paramêtros<a class="anchorjs-link " href="#background-gradient" aria-label="Anchor" data-anchorjs-icon="#" style="padding-left: 0.375em;"></a></span></h2>

<p> Lista de parametêtros utilizados no componente</p>

<ul>
  <li><code class="highlighter-rouge">corrossel_botoes</code> -  Função utilizada para Ativar os botões de controle do Carrossel (desabilitados por deafult)</li>
  <li><code class="highlighter-rouge">setImg</code> - Parametro utilizado para definir o número da imagem utilizada</li>
  <li><code class="highlighter-rouge">setContent</code> - Função utilizada para instanciar o novo item</li>
</ul>

<br>

<section>

<!-- mdbsnippet --><div class="docs-pills container mx-auto mdb-color lighten-5 mt-3">  <div class="d-flex justify-content-between">    <ul class="nav md-pills pills-grey"><li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#mdb_e22d94e770bd0faa56974bf8a4eba056" role="tab">HTML</a></li></ul></div><div class="tab-content"><div class="tab-pane fade active show" id="mdb_e22d94e770bd0faa56974bf8a4eba056" role="tabpanel"><div class="code-toolbar"><pre class="grey lighten-3 px-3 mb-0 line-numbers  language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carouselExampleSlidesOnly<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carousel slide<span class="token punctuation">"</span></span> <span class="token attr-name">data-ride</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carousel<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carousel-inner<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carousel-item active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>img</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>d-block w-100<span class="token punctuation">"</span></span> <span class="token attr-name">src</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>https://mdbootstrap.com/img/Photos/Slides/img%20(35).jpg<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carousel-item<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>img</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>d-block w-100<span class="token punctuation">"</span></span> <span class="token attr-name">src</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>https://mdbootstrap.com/img/Photos/Slides/img%20(33).jpg<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>carousel-item<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>img</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>d-block w-100<span class="token punctuation">"</span></span> <span class="token attr-name">src</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>https://mdbootstrap.com/img/Photos/Slides/img%20(31).jpg<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span><span aria-hidden="true" class="line-numbers-rows"><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span></code></pre><div class="toolbar"><div class="toolbar-item"></div><div class="toolbar-item"></div></div></div></div></div></div><!-- /.mdbsnippet -->

</section>
</div>