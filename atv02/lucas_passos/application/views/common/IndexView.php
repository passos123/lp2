<br><br><div class="class mt-5">
<!-- Card group -->
<div class="card-group p-5">

  <!-- Card -->
  <div class="card mb-4">

    <!-- Card image -->
    <div class="view overlay">
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!-- Card content -->
    <div class="card-body">

      <!-- Title -->
      <h4 class="card-title">Componente Carrossel</h4>
      <!-- Text -->
      <p class="card-text">Venha aprender a utilizar esse inovador componente.</p>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
      <a href="<?php echo site_url('Carrossel')?>"><button type="button" class="btn btn-dark btn-md">Ler mais</button></a>

    </div>
    <!-- Card content -->

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!-- Card image -->
    <div class="view overlay">
      
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!-- Card content -->
    <div class="card-body">
      <!-- Title -->
      <h4 class="card-title">Componente ComingSoon</h4>
      <!-- Text -->
      <p class="card-text">Venha aprender a utilizar esse inovador componente.</p>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
      <a href="<?php echo site_url('Comingsoon')?>"><button type="button" class="btn btn-dark btn-md">Ler mais</button></a>

    </div>
    <!-- Card content -->

  </div>
  <!-- Card -->

  <!-- Card -->
  <div class="card mb-4">

    <!-- Card image -->
    <div class="view overlay">
      
      <a href="#!">
        <div class="mask rgba-white-slight"></div>
      </a>
    </div>

    <!-- Card content -->
    <div class="card-body">

      <!-- Title -->
      <h4 class="card-title">Componente Navbar</h4>
      <!-- Text -->
      <p class="card-text">Venha aprender a utilizar esse inovador componente.</p>
      <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
      <a href="<?php echo site_url('Navbar')?>"><button type="button" class="btn btn-dark btn-md">Ler mais</button></a>

    </div>
    <!-- Card content -->

  </div>
  <!-- Card -->

</div>
<!-- Card group -->
</div>