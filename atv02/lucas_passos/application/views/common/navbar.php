<!--Main Navigation-->
<header>

<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar navbar-dark elegant-color-dark">
  <div class="container">

    <!-- Brand -->
    <a class="navbar-brand waves-effect" href="<?php echo site_url('index')?>">
      <strong class="white-text">Componentes</strong>
    </a>

    <!-- Collapse -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Links -->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <!-- Left -->
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link waves-effect" href="<?php echo site_url('index')?>">Home
            <span class="sr-only">(current)</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect"  href="<?php echo site_url('Carrossel')?>">Carrossel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect"  href="<?php echo site_url('Navbar')?>">Navbar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect"  href="<?php echo site_url('Comingsoon')?>">Comingsoon</a>
        </li>
        <li class="nav-item">
          <a class="nav-link waves-effect "  href="<?php echo site_url('docs/api')?>" target="_blank">Documentação</a>
        </li>
      </ul>



    </div>

  </div>
</nav>
<!-- Navbar -->

</header>
<!--Main Navigation-->