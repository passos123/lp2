<br><br><div class="container mt-5">
<div class="d-flex justify-content-end"><a href="<?php echo site_url('/test/Navbartest')?>"><button type="button" class="btn-sm btn-dark">Teste unitário</button></a></div>

<h1 class="bd-title" id="content">NavBar</h1>

          <p class="bd-lead">Documentação e exemplos para o poderoso cabeçalho de navegação responsivo do Bootstrap, a barra de navegação.</p>
<?php
echo $navbar;
?>
<div class="container mx-auto mdb-color lighten-5 mt-3">
<code class="language-html" data-lang="html">
<p>Exemplo de utilização:</p>
<span>setTitle('Novo Titulo')->setHref('exemplohref')->addTitleLink('titulo1alterado')->link()->setTitle('Titulo 2')->addTitleLink('titulo2alterado')->link()->addTitleLink('link 2')->link()->setColor('info-color-dark')->setTextColor('navbar-dark');
</span></code>
</div>
<h5 class="bd-lead mt-2">Exemplo de uma navbar genérica:</h5>
    <div class="container mx-auto mdb-color lighten-5 mt-3">
        <pre>
<code class="language-html" data-lang="html"><span class="nt">&lt;nav</span> <span class="na">class=</span><span class="s">"navbar navbar-expand-lg navbar-light bg-light"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"navbar-brand"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Navbar<span class="nt">&lt;/a&gt;</span>
  <span class="nt">&lt;button</span> <span class="na">class=</span><span class="s">"navbar-toggler"</span> <span class="na">type=</span><span class="s">"button"</span> <span class="na">data-toggle=</span><span class="s">"collapse"</span> <span class="na">data-target=</span><span class="s">"#navbarSupportedContent"</span> <span class="na">aria-controls=</span><span class="s">"navbarSupportedContent"</span> <span class="na">aria-expanded=</span><span class="s">"false"</span> <span class="na">aria-label=</span><span class="s">"Toggle navigation"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"navbar-toggler-icon"</span><span class="nt">&gt;&lt;/span&gt;</span>
  <span class="nt">&lt;/button&gt;</span>

  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"collapse navbar-collapse"</span> <span class="na">id=</span><span class="s">"navbarSupportedContent"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;ul</span> <span class="na">class=</span><span class="s">"navbar-nav mr-auto"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"nav-item active"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"nav-link"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Home <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"sr-only"</span><span class="nt">&gt;</span>(current)<span class="nt">&lt;/span&gt;&lt;/a&gt;</span>
      <span class="nt">&lt;/li&gt;</span>
      <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"nav-item"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"nav-link"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Link<span class="nt">&lt;/a&gt;</span>
      <span class="nt">&lt;/li&gt;</span>
      <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"nav-item dropdown"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"nav-link dropdown-toggle"</span> <span class="na">href=</span><span class="s">"#"</span> <span class="na">id=</span><span class="s">"navbarDropdown"</span> <span class="na">role=</span><span class="s">"button"</span> <span class="na">data-toggle=</span><span class="s">"dropdown"</span> <span class="na">aria-haspopup=</span><span class="s">"true"</span> <span class="na">aria-expanded=</span><span class="s">"false"</span><span class="nt">&gt;</span>
          Dropdown
        <span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"dropdown-menu"</span> <span class="na">aria-labelledby=</span><span class="s">"navbarDropdown"</span><span class="nt">&gt;</span>
          <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"dropdown-item"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Action<span class="nt">&lt;/a&gt;</span>
          <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"dropdown-item"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Another action<span class="nt">&lt;/a&gt;</span>
          <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"dropdown-divider"</span><span class="nt">&gt;&lt;/div&gt;</span>
          <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"dropdown-item"</span> <span class="na">href=</span><span class="s">"#"</span><span class="nt">&gt;</span>Something else here<span class="nt">&lt;/a&gt;</span>
        <span class="nt">&lt;/div&gt;</span>
      <span class="nt">&lt;/li&gt;</span>
      <span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">"nav-item"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;a</span> <span class="na">class=</span><span class="s">"nav-link disabled"</span> <span class="na">href=</span><span class="s">"#"</span> <span class="na">tabindex=</span><span class="s">"-1"</span> <span class="na">aria-disabled=</span><span class="s">"true"</span><span class="nt">&gt;</span>Disabled<span class="nt">&lt;/a&gt;</span>
      <span class="nt">&lt;/li&gt;</span>
    <span class="nt">&lt;/ul&gt;</span>
    <span class="nt">&lt;form</span> <span class="na">class=</span><span class="s">"form-inline my-2 my-lg-0"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;input</span> <span class="na">class=</span><span class="s">"form-control mr-sm-2"</span> <span class="na">type=</span><span class="s">"search"</span> <span class="na">placeholder=</span><span class="s">"Search"</span> <span class="na">aria-label=</span><span class="s">"Search"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;button</span> <span class="na">class=</span><span class="s">"btn btn-outline-success my-2 my-sm-0"</span> <span class="na">type=</span><span class="s">"submit"</span><span class="nt">&gt;</span>Search<span class="nt">&lt;/button&gt;</span>
    <span class="nt">&lt;/form&gt;</span>
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/nav&gt;</span></code>
        </pre>
    </div>
</div>

<div class="container mx-auto mt-3">
<h2 id="background-gradient"><span class="bd-content-title"> Paramêtros<a class="anchorjs-link " href="#background-gradient" aria-label="Anchor" data-anchorjs-icon="#" style="padding-left: 0.375em;"></a></span></h2>

<p> Lista de parametêtros utilizados no componente</p>

<ul>
  <li><code class="highlighter-rouge">setTitle</code> -  Pametro utilizado para definir o Título da Navbar</li>
  <li><code class="highlighter-rouge">addTitleLink</code> - Adiciona o nome de um link na Navbar</li>
  <li><code class="highlighter-rouge">link</code> - Utilizado para instanciar um novo link</li>
  <li><code class="highlighter-rouge">setHref</code> - Utilizado para definir o endereço do link</li>
  <li><code class="highlighter-rouge">setColor</code> - Muda a cor da NavBar</li>
  <li><code class="highlighter-rouge">setTextColor</code> - Muda a cor do texto</li>

</ul>

<section class="documentation">
<style>
  .color-block h5 {
      font-size: 1rem;
  }
  .dynamic-color p {
      font-size: 0.75rem
  }
</style>

<!--Section: Intro-->
<section id="introduction">

  <!--Title-->
  <h2 class="primary-heading">Cores aceitas</h2>

 

  <!--Description-->
  <p>As cores do Bootstrap são uma paleta sensacional de 300 cores deliciosas, que o ajudarão a criar um design convidativo e consistente.</p>

  <p>Cada cor pode ser exposta em vários tons, variando de claro a escuro. Todos eles são agrupados em uma seção acessível, o que ajudará você a criar um design convidativo e consistente em cores.</p>
  <!--Section: Live preview-->
  <section>

    <div class="row mx-1 pt-3">
        <div class="col-md-3 mb-4">
            <div class="color-block danger-color z-depth-2">
                <h5>danger-color</h5>
                <h5>#ff4444</h5>
            </div>
            <div class="color-block-dark danger-color-dark z-depth-2">
                <h5>danger-color-dark</h5>
                <h5>#CC0000</h5>
            </div>

        </div>

        <div class="col-md-3 mb-4">
            <div class="color-block warning-color z-depth-2">
                <h5>warning-color</h5>
                <h5>#ffbb33</h5>
            </div>
            <div class="color-block-dark warning-color-dark z-depth-2">
                <h5>warning-color-dark</h5>
                <h5>#FF8800</h5>
            </div>

        </div>

        <div class="col-md-3 mb-4">
            <div class="color-block success-color z-depth-2">
                <h5>success-color</h5>
                <h5>#00C851</h5>
            </div>
            <div class="color-block-dark success-color-dark z-depth-2">
                <h5>success-color-dark</h5>
                <h5>#007E33</h5>
            </div>

        </div>

        <div class="col-md-3 mb-4">

            <div class="color-block info-color z-depth-2">
                <h5>info-color</h5>
                <h5>#33b5e5</h5>
            </div>
            <div class="color-block-dark info-color-dark z-depth-2">
                <h5>info-color-dark</h5>
                <h5>#0099CC</h5>
            </div>
        </div>
    </div>

  </section>
  <!--Section: Live preview-->

</section>


<section class="documentation">
                            <style>
  .primary-lighter-hover {
    color: #4285F4;
    -webkit-transition: .4s;
    transition: .4s; }
    .primary-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #8ab4f8; }

  .primary-darker-hover {
    color: #4285F4;
    -webkit-transition: .4s;
    transition: .4s; }
    .primary-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #0d5bdd; }

  .danger-lighter-hover {
    color: #ff3547;
    -webkit-transition: .4s;
    transition: .4s; }
    .danger-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #ff828d; }

  .danger-darker-hover {
    color: #ff3547;
    -webkit-transition: .4s;
    transition: .4s; }
    .danger-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #e80015; }

  .warning-lighter-hover {
    color: #FF8800;
    -webkit-transition: .4s;
    transition: .4s; }
    .warning-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #ffac4d; }

  .warning-darker-hover {
    color: #FF8800;
    -webkit-transition: .4s;
    transition: .4s; }
    .warning-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #b35f00; }

  .success-lighter-hover {
    color: #00C851;
    -webkit-transition: .4s;
    transition: .4s; }
    .success-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #16ff74; }

  .success-darker-hover {
    color: #00C851;
    -webkit-transition: .4s;
    transition: .4s; }
    .success-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #007c32; }

  .info-lighter-hover {
    color: #33b5e5;
    -webkit-transition: .4s;
    transition: .4s; }
    .info-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #77ceee; }

  .info-darker-hover {
    color: #33b5e5;
    -webkit-transition: .4s;
    transition: .4s; }
    .info-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #178ab4; }

  .default-lighter-hover {
    color: #2BBBAD;
    -webkit-transition: .4s;
    transition: .4s; }
    .default-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #5ad9cd; }

  .default-darker-hover {
    color: #2BBBAD;
    -webkit-transition: .4s;
    transition: .4s; }
    .default-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #1d7d73; }

  .secondary-lighter-hover {
    color: #aa66cc;
    -webkit-transition: .4s;
    transition: .4s; }
    .secondary-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #ca9fdf; }

  .secondary-darker-hover {
    color: #aa66cc;
    -webkit-transition: .4s;
    transition: .4s; }
    .secondary-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #8639ac; }

  .elegant-lighter-hover {
    color: #2E2E2E;
    -webkit-transition: .4s;
    transition: .4s; }
    .elegant-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #545454; }

  .elegant-darker-hover {
    color: #2E2E2E;
    -webkit-transition: .4s;
    transition: .4s; }
    .elegant-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #080808; }

  .unique-lighter-hover {
    color: #880e4f;
    -webkit-transition: .4s;
    transition: .4s; }
    .unique-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #cd1577; }

  .unique-darker-hover {
    color: #880e4f;
    -webkit-transition: .4s;
    transition: .4s; }
    .unique-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #430727; }

  .dark-green-lighter-hover {
    color: #388E3C;
    -webkit-transition: .4s;
    transition: .4s; }
    .dark-green-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #56bc5b; }

  .dark-green-darker-hover {
    color: #388E3C;
    -webkit-transition: .4s;
    transition: .4s; }
    .dark-green-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #225725; }

  .mdb-color-lighter-hover {
    color: #59698D;
    -webkit-transition: .4s;
    transition: .4s; }
    .mdb-color-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #8290b0; }

  .mdb-color-darker-hover {
    color: #59698D;
    -webkit-transition: .4s;
    transition: .4s; }
    .mdb-color-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #3b465e; }

  .red-lighter-hover {
    color: #D32F2F;
    -webkit-transition: .4s;
    transition: .4s; }
    .red-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #e06e6e; }

  .red-darker-hover {
    color: #D32F2F;
    -webkit-transition: .4s;
    transition: .4s; }
    .red-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #962020; }

  .pink-lighter-hover {
    color: #ec407a;
    -webkit-transition: .4s;
    transition: .4s; }
    .pink-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #f386aa; }

  .pink-darker-hover {
    color: #ec407a;
    -webkit-transition: .4s;
    transition: .4s; }
    .pink-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #cb1452; }

  .purple-lighter-hover {
    color: #8e24aa;
    -webkit-transition: .4s;
    transition: .4s; }
    .purple-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #b843d7; }

  .purple-darker-hover {
    color: #8e24aa;
    -webkit-transition: .4s;
    transition: .4s; }
    .purple-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #59176b; }

  .deep-purple-lighter-hover {
    color: #512da8;
    -webkit-transition: .4s;
    transition: .4s; }
    .deep-purple-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #7651d0; }

  .deep-purple-darker-hover {
    color: #512da8;
    -webkit-transition: .4s;
    transition: .4s; }
    .deep-purple-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #341d6c; }

  .indigo-lighter-hover {
    color: #3f51b5;
    -webkit-transition: .4s;
    transition: .4s; }
    .indigo-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #7280ce; }

  .indigo-darker-hover {
    color: #3f51b5;
    -webkit-transition: .4s;
    transition: .4s; }
    .indigo-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #2b387c; }

  .blue-lighter-hover {
    color: #1976D2;
    -webkit-transition: .4s;
    transition: .4s; }
    .blue-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #4e9cea; }

  .blue-darker-hover {
    color: #1976D2;
    -webkit-transition: .4s;
    transition: .4s; }
    .blue-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #11508e; }

  .light-blue-lighter-hover {
    color: #82B1FF;
    -webkit-transition: .4s;
    transition: .4s; }
    .light-blue-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #cfe1ff; }

  .light-blue-darker-hover {
    color: #82B1FF;
    -webkit-transition: .4s;
    transition: .4s; }
    .light-blue-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #3681ff; }

  .cyan-lighter-hover {
    color: #00bcd4;
    -webkit-transition: .4s;
    transition: .4s; }
    .cyan-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #22e6ff; }

  .cyan-darker-hover {
    color: #00bcd4;
    -webkit-transition: .4s;
    transition: .4s; }
    .cyan-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #007888; }

  .teal-lighter-hover {
    color: #00796b;
    -webkit-transition: .4s;
    transition: .4s; }
    .teal-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #00c6af; }

  .teal-darker-hover {
    color: #00796b;
    -webkit-transition: .4s;
    transition: .4s; }
    .teal-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #002d27; }

  .green-lighter-hover {
    color: #388E3C;
    -webkit-transition: .4s;
    transition: .4s; }
    .green-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #56bc5b; }

  .green-darker-hover {
    color: #388E3C;
    -webkit-transition: .4s;
    transition: .4s; }
    .green-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #225725; }

  .light-green-lighter-hover {
    color: #8bc34a;
    -webkit-transition: .4s;
    transition: .4s; }
    .light-green-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #b0d683; }

  .light-green-darker-hover {
    color: #8bc34a;
    -webkit-transition: .4s;
    transition: .4s; }
    .light-green-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #649130; }

  .lime-lighter-hover {
    color: #afb42b;
    -webkit-transition: .4s;
    transition: .4s; }
    .lime-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #d2d655; }

  .lime-darker-hover {
    color: #afb42b;
    -webkit-transition: .4s;
    transition: .4s; }
    .lime-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #73761c; }

  .yellow-lighter-hover {
    color: #fbc02d;
    -webkit-transition: .4s;
    transition: .4s; }
    .yellow-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #fcd778; }

  .yellow-darker-hover {
    color: #fbc02d;
    -webkit-transition: .4s;
    transition: .4s; }
    .yellow-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #d79b04; }

  .amber-lighter-hover {
    color: #ffa000;
    -webkit-transition: .4s;
    transition: .4s; }
    .amber-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #ffbd4d; }

  .amber-darker-hover {
    color: #ffa000;
    -webkit-transition: .4s;
    transition: .4s; }
    .amber-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #b37000; }

  .orange-lighter-hover {
    color: #f57c00;
    -webkit-transition: .4s;
    transition: .4s; }
    .orange-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #ffa243; }

  .orange-darker-hover {
    color: #f57c00;
    -webkit-transition: .4s;
    transition: .4s; }
    .orange-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #a95500; }

  .deep-orange-lighter-hover {
    color: #ff7043;
    -webkit-transition: .4s;
    transition: .4s; }
    .deep-orange-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #ffaa90; }

  .deep-orange-darker-hover {
    color: #ff7043;
    -webkit-transition: .4s;
    transition: .4s; }
    .deep-orange-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #f63b00; }

  .brown-lighter-hover {
    color: #795548;
    -webkit-transition: .4s;
    transition: .4s; }
    .brown-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #a57868; }

  .brown-darker-hover {
    color: #795548;
    -webkit-transition: .4s;
    transition: .4s; }
    .brown-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #49332b; }

  .grey-lighter-hover {
    color: #616161;
    -webkit-transition: .4s;
    transition: .4s; }
    .grey-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #878787; }

  .grey-darker-hover {
    color: #616161;
    -webkit-transition: .4s;
    transition: .4s; }
    .grey-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #3b3b3b; }

  .blue-grey-lighter-hover {
    color: #78909c;
    -webkit-transition: .4s;
    transition: .4s; }
    .blue-grey-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #a4b4bc; }

  .blue-grey-darker-hover {
    color: #78909c;
    -webkit-transition: .4s;
    transition: .4s; }
    .blue-grey-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #546973; }

  .white-lighter-hover {
    color: #fff;
    -webkit-transition: .4s;
    transition: .4s; }
    .white-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: white; }

  .white-darker-hover {
    color: #fff;
    -webkit-transition: .4s;
    transition: .4s; }
    .white-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #d9d9d9; }

  .black-lighter-hover {
    color: #000;
    -webkit-transition: .4s;
    transition: .4s; }
    .black-lighter-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: #262626; }

  .black-darker-hover {
    color: #000;
    -webkit-transition: .4s;
    transition: .4s; }
    .black-darker-hover:hover {
      -webkit-transition: .4s;
      transition: .4s;
      color: black; }

  .scrollspy-example {
    height: 200px;
  }

  .docs-tabs {
    margin-top: 1rem;
  }

  .docs-tabs .nav-link {
    border-width: 1px 1px 0 1px;
    border-color: rgba(0, 0, 0, .15);
    border-style: solid;
    border-radius: 5px 5px 0 0;
    margin-right: 8px;
    margin-bottom: -1px;
    color: #212529;
  }

  .docs-tabs .nav-link.active {
    font-weight: 600;
    background: #eaeaea;
  }

  .docs-tab-content {
    border: 1px solid rgba(0, 0, 0, .15);
    padding: 0;
  }

  .docs-tab-content pre[class*=language-] {
    margin: 0 !important;
    box-shadow: none !important;
  }
</style>
<br><br>
<!--Section: Intro-->
<section id="introduction">

  <!--Title-->
  <h2 class="primary-heading">Text color</h2>



  <!--Description-->
  <p>A cor do texto de inicialização é um conjunto de cores que pode ser usado para alterar a cor da fonte. A cor também pode ser alterada ao passar o mouse sobre uma área de texto.</p>

</section>

<!--Section: -->
<section id="material">

  <!--Title-->
  <h2 class="section-heading">
    Material text color
  </h2>


  <!--Section: Live preview-->
  <section class="section-preview font-weight-bold">

      <p class="text-default">.text-default</p>
      <p class="text-primary">.text-primary</p>
      <p class="text-secondary">.text-secondary</p>
      <p class="text-success">.text-success</p>
      <p class="text-danger">.text-danger</p>
      <p class="text-warning">.text-warning</p>
      <p class="text-info">.text-info</p>

  </section>
  <!--Section: Live preview-->


</section>
<!--/Section: -->
</div>