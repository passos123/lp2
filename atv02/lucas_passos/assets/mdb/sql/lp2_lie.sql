-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Nov-2019 às 02:13
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_lie`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `chat_item`
--

CREATE TABLE `chat_item` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `conteudo` text,
  `user_id` mediumint(9) DEFAULT NULL,
  `chat_list_id` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `chat_item`
--

INSERT INTO `chat_item` (`id`, `conteudo`, `user_id`, `chat_list_id`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 7, 1),
(2, 'Lorem ipsum dolor sit amet,', 6, 5),
(3, 'Lorem ipsum dolor', 3, 3),
(4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 7, 4),
(5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 5, 3),
(6, 'Lorem ipsum dolor sit', 7, 5),
(7, 'Lorem ipsum dolor sit amet, consectetuer', 1, 4),
(8, 'Lorem ipsum dolor sit amet, consectetuer', 4, 2),
(9, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 2, 3),
(10, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 2, 2),
(11, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 8, 5),
(12, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 6, 5),
(13, 'Lorem ipsum dolor sit amet,', 8, 4),
(14, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 1, 3),
(15, 'Lorem ipsum dolor', 2, 2),
(16, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 4, 2),
(17, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 5, 4),
(18, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 5, 2),
(19, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 5, 5),
(20, 'Lorem ipsum dolor', 7, 2),
(21, 'Lorem ipsum dolor sit amet, consectetuer', 6, 4),
(22, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 1, 4),
(23, 'Lorem ipsum dolor', 5, 4),
(24, 'Lorem ipsum dolor sit', 5, 3),
(25, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 1, 3),
(26, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 6, 4),
(27, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 5, 5),
(28, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 3, 4),
(29, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 10, 3),
(30, 'Lorem ipsum dolor sit', 10, 4),
(31, 'Lorem ipsum dolor sit amet,', 10, 1),
(32, 'Lorem ipsum dolor sit amet, consectetuer', 10, 2),
(33, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 9, 4),
(34, 'Lorem ipsum dolor sit amet, consectetuer', 2, 1),
(35, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 6, 1),
(36, 'Lorem ipsum dolor sit', 9, 1),
(37, 'Lorem ipsum dolor', 5, 2),
(38, 'Lorem ipsum dolor sit amet,', 5, 5),
(39, 'Lorem ipsum dolor sit', 9, 5),
(40, 'Lorem ipsum dolor sit amet,', 9, 4),
(41, 'Lorem ipsum dolor sit amet, consectetuer', 4, 1),
(42, 'Lorem ipsum dolor sit', 8, 5),
(43, 'Lorem ipsum dolor sit amet,', 4, 5),
(44, 'Lorem ipsum dolor sit amet, consectetuer', 3, 3),
(45, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 7, 4),
(46, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 8, 3),
(47, 'Lorem ipsum dolor sit amet,', 7, 2),
(48, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 4, 3),
(49, 'Lorem ipsum dolor sit amet, consectetuer', 3, 5),
(50, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 1, 2),
(51, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 7, 1),
(52, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 8, 1),
(53, 'Lorem ipsum dolor', 5, 1),
(54, 'Lorem ipsum dolor sit', 9, 5),
(55, 'Lorem ipsum dolor', 5, 2),
(56, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 10, 2),
(57, 'Lorem ipsum dolor', 8, 1),
(58, 'Lorem ipsum dolor sit amet,', 1, 3),
(59, 'Lorem ipsum dolor sit amet,', 2, 4),
(60, 'Lorem ipsum dolor', 7, 2),
(61, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 3, 5),
(62, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 10, 4),
(63, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 8, 4),
(64, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 9, 4),
(65, 'Lorem ipsum dolor', 8, 1),
(66, 'Lorem ipsum dolor', 3, 4),
(67, 'Lorem ipsum dolor sit amet, consectetuer', 10, 4),
(68, 'Lorem ipsum dolor sit', 5, 4),
(69, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 2, 5),
(70, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 6, 3),
(71, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 6, 4),
(72, 'Lorem ipsum dolor sit amet, consectetuer', 5, 5),
(73, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 2, 2),
(74, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 9, 4),
(75, 'Lorem ipsum dolor sit', 2, 4),
(76, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 8, 3),
(77, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 3, 2),
(78, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 5, 5),
(79, 'Lorem ipsum dolor', 9, 4),
(80, 'Lorem ipsum dolor', 1, 4),
(81, 'Lorem ipsum dolor sit amet, consectetuer', 9, 3),
(82, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 2, 2),
(83, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 2, 1),
(84, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 4, 2),
(85, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 2, 1),
(86, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 8, 4),
(87, 'Lorem ipsum dolor sit amet,', 5, 2),
(88, 'Lorem ipsum dolor', 5, 3),
(89, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 7, 4),
(90, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 8, 4),
(91, 'Lorem ipsum dolor sit', 6, 1),
(92, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 7, 4),
(93, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur', 5, 3),
(94, 'Lorem ipsum dolor sit amet,', 1, 2),
(95, 'Lorem ipsum dolor', 2, 1),
(96, 'Lorem ipsum dolor sit amet, consectetuer adipiscing', 10, 1),
(97, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 10, 5),
(98, 'Lorem ipsum dolor sit amet,', 7, 1),
(99, 'Lorem ipsum dolor sit amet, consectetuer', 3, 3),
(100, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed', 1, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `chat_list`
--

CREATE TABLE `chat_list` (
  `id` int(11) NOT NULL,
  `tema` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `chat_list`
--

INSERT INTO `chat_list` (`id`, `tema`) VALUES
(1, 'Política'),
(2, 'Religião'),
(3, 'Futebol'),
(4, 'Estilo Musical'),
(5, 'Informática');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chat_user`
--

CREATE TABLE `chat_user` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `sobrenome` varchar(255) DEFAULT NULL,
  `imagem` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `chat_user`
--

INSERT INTO `chat_user` (`id`, `nome`, `sobrenome`, `imagem`) VALUES
(1, 'Roth', 'Ware', 158),
(2, 'Xenos', 'Holder', 822),
(3, 'Merritt', 'Mason', 213),
(4, 'Mallory', 'Chaney', 607),
(5, 'Beck', 'Grimes', 555),
(6, 'Eliana', 'Murray', 841),
(7, 'Justin', 'Nelson', 701),
(8, 'Martena', 'Patterson', 885),
(9, 'Christen', 'Henderson', 237),
(10, 'Eugenia', 'Wade', 649);

-- --------------------------------------------------------

--
-- Estrutura da tabela `dados_pessoais`
--

CREATE TABLE `dados_pessoais` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `sexo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dados_pessoais`
--

INSERT INTO `dados_pessoais` (`id`, `nome`, `sobrenome`, `sexo`) VALUES
(1, 'Zero', 'Shirotsuki', 2),
(2, 'Shin', 'Sakamitsu', 2),
(3, 'Lúpus', 'Remmiki', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id` int(11) NOT NULL,
  `id_funcionario` int(11) NOT NULL,
  `rua` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`id`, `id_funcionario`, `rua`, `bairro`, `cidade`, `estado`) VALUES
(1, 1, 'Av. Salgado Filho, 2000', 'Vila Rio de Janeiro', 'Guarulhos', 'São Paulo'),
(2, 2, 'Av. Azedo Filho', 'Vila Rio de Janeiro', 'Guarulhos', 'São Paulo'),
(3, 3, 'Av. Tiradentes, 1000', 'Centro', 'São Paulo', 'São Paulo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `preco` decimal(8,2) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `descricao`, `preco`, `last_modified`) VALUES
(1, 'Teste', 'Um teste qualquer feito para ver se está tudo funcionando perfeitamente', '18.90', '2019-09-23 14:32:03'),
(4, 'Sabão', 'Passar no corpo', '6.77', '2019-09-10 00:40:41'),
(8, 'Sapatos', 'Para proteger os pés', '5.86', '2019-09-10 01:15:06'),
(9, 'Ut Nec Incorporated', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius', '8.26', '2019-09-02 23:39:40'),
(10, 'Enim Corp.', 'sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl', '8.67', '2019-09-02 23:39:40'),
(11, 'Quisque Varius Assoc', 'sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur', '7.78', '2019-09-02 23:39:40'),
(12, 'Egestas Aliquam PC', 'eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec', '4.05', '2019-09-02 23:39:40'),
(13, 'Magnis Company', 'nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis', '3.15', '2019-09-02 23:39:40'),
(14, 'Et Company', 'libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis,', '2.29', '2019-09-02 23:39:40'),
(15, 'Donec At Corp.', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et,', '6.23', '2019-09-02 23:39:40'),
(16, 'Purus Nullam Limited', 'neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi', '9.65', '2019-09-02 23:39:40'),
(17, 'Rhoncus Proin Consul', 'sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus', '1.27', '2019-09-02 23:39:40'),
(18, 'Suspendisse Ltd', 'egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam', '6.68', '2019-09-02 23:39:40'),
(19, 'Sed LLC', 'lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc', '1.29', '2019-09-02 23:39:40'),
(20, 'Vestibulum Accumsan ', 'vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc', '1.38', '2019-09-02 23:39:40'),
(21, 'Salame', 'Carne processada', '20.00', '2019-09-09 23:26:07'),
(22, 'Abacaxi', 'Uma fruta amarela extremamente deliciosa', '18.00', '2019-10-03 04:51:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat_item`
--
ALTER TABLE `chat_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_list`
--
ALTER TABLE `chat_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user`
--
ALTER TABLE `chat_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat_item`
--
ALTER TABLE `chat_item`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `chat_list`
--
ALTER TABLE `chat_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chat_user`
--
ALTER TABLE `chat_user`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
