<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Validator extends CI_Object{
    
    public function produto_validate() {
        $this->form_validation->set_rules('nome', 'Nome do produto', 'trim|required|min_length[4]|max_length[40]');
        $this->form_validation->set_rules('descricao', 'Descrição do produto', 'trim|required|min_length[10]|max_length[100]');
        $this->form_validation->set_rules('preco', 'Preço do produto', 'trim|required|decimal|greater_than[0]');
        return $this->form_validation->run();
    }

    public function valida_dados_pessoais() {
        $this->form_validation->set_rules('dados[nome]', 'Nome do funcionário', 'trim|required|min_length[2]|max_length[20]');    
        $this->form_validation->set_rules('dados[sobrenome]', 'Sobrenome do funcionário', 'trim|required|min_length[4]|max_length[80]');    
        $this->form_validation->set_rules('dados[sexo]', 'Gênero do funcionário', 'required|in_list[1,2]');    
    }

    public function valida_endereco() {
        $this->form_validation->set_rules('endereco[rua]', 'Rua', 'trim|required|min_length[4]|max_length[100]');
        $this->form_validation->set_rules('endereco[bairro]', 'Bairro', 'trim|required|min_length[4]|max_length[100]');
        $this->form_validation->set_rules('endereco[cidade]', 'Cidade', 'trim|required|min_length[4]|max_length[100]');
        $this->form_validation->set_rules('endereco[estado]', 'Estado', 'trim|required|min_length[4]|max_length[100]');
    }

}
